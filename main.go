package main

import (
	"fmt"
	"net/http"

	"gitlab.com/gitops-demo/apps/my-go-app5/v2/hello"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, you've requested: %s\n", r.URL.Path)
		fmt.Fprintf(w, "Proverb: %s\n", hello.Proverb())
	})

	http.ListenAndServe(":5000", nil)
}
